<img style="background: #000000; padding: 12px 10px 10px 10px" src="https://grappus.com/static/images/home/nav/grappus-logo.svg" alt="grappus" width="100" />

# Grappus Base Repo (React)

### Libraries

- **UI Library**: Antd [(link)](https://ant.design/)
- **Framework** : React

### Project Setup

- Clone the project [repository]() from Gitlab
- After cloning, get into the project folder
  ```sh
  $ cd grappus-base-csm
  ```
- Install all dependencies
  ```sh
  $ yarn
  ```
- Run the app
  ```sh
  $ yarn start
  ```
- App is now running at
  ```
  http://localhost:3000/
  ```

### Authors

##### Deepak Bhardwaj : deepak@grappus.com [(github)](github.com/kapeed07)
