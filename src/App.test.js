import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders Hello from Grappus base repo :)", () => {
  render(<App />);
  const linkElement = screen.getByText("Hello from Grappus base repo :)");
  expect(linkElement).toBeInTheDocument();
});
