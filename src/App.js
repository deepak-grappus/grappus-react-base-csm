import { Layout, Menu } from 'antd';
import React from 'react';
import logo from './assets/images/logo.svg';

const { Header, Content } = Layout;

function App() {
  return (
    <Layout>
      <Header className="flex items-center justify-between shadow-md sticky top-0">
        <img src={logo} alt="grappus" width="100" />
        <div>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">Home</Menu.Item>
            <Menu.Item key="2">Lab</Menu.Item>
            <Menu.Item key="3">Playground</Menu.Item>
            <Menu.Item key="4">Settings</Menu.Item>
          </Menu>
        </div>
        <div className="text-white cursor-pointer">Logout</div>
      </Header>
      <Content className="bg-gray-50">
        <div style={{ padding: 24, minHeight: '100vh' }}>
          <div className="h-screen flex flex-col items-center justify-center">
            <img
              className="bg-black pt-4 pb-3 px-4 mb-2"
              src={logo}
              alt="grappus"
              width="200"
            />
            <div className="text-4xl font-light">base react repo... 👨🏻‍💻</div>
            <div className="text-lg font-thin">create a kickass project</div>
            <div className="text-gray-500 p-3 fixed bottom-0 left-0 text-xs">
              Authors: Deepak Bhardwaj
            </div>
          </div>
        </div>
      </Content>
    </Layout>
  );
}

export default App;
